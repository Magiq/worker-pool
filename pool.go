package worker

import (
	"errors"
	"sync"
	"sync/atomic"
	"time"
)

type flushFn func(*JobState)

var (
	ErrJobNotFound   = errors.New("Job not found")
	ErrJobIsRunning  = errors.New("Job is running")
	PoolIsRunning    = errors.New("Pool is running")
	PoolIsNotRunning = errors.New("Pool is not running")
)

type Pool struct {
	jobs        []*Job
	numWorkers  int
	jobQueue    chan *Job
	outputQueue chan *JobState
	running     uint32
	flush       *func(map[int64]*JobState)
	sync.RWMutex
}

func CreatePool(numWorkers int, jobQueueCount int, flush *func(map[int64]*JobState)) *Pool {
	return &Pool{
		jobQueue:    make(chan *Job, jobQueueCount),
		outputQueue: make(chan *JobState),
		flush:       flush,
		numWorkers:  numWorkers,
	}
}

func (p *Pool) Run() error {
	if p.isRunning() {
		return PoolIsRunning
	}

	for i := 0; i < p.numWorkers; i++ {
		go runWorker(p.jobQueue, p.outputQueue)
	}

	if p.flush != nil {
		go p.statDaemon()
	} else {
		go p.blackHole()
	}

	return nil
}

func (p *Pool) Stop() error {
	if !p.isRunning() {
		return PoolIsNotRunning
	}

	close(p.jobQueue)
	close(p.outputQueue)

	p.FlushJob()

	return nil
}

func (p *Pool) FlushJob() {
	for {
		_, jobOpen := <-p.jobQueue
		if !jobOpen {
			break
		}
	}
}

func (p *Pool) Exec(taskID int64, names, tags []string) {
	jobs := p.getJobs(names, tags)

	for _, job := range jobs {
		if !job.isRunning() {
			job.TaskID = taskID
			p.jobQueue <- job
		}
	}
}

func (p *Pool) AddJob(job *Job) {
	p.Lock()
	defer p.Unlock()

	p.jobs = append(p.jobs, job)
}

func (p *Pool) getJobs(names, tags []string) []*Job {
	p.RLock()
	defer p.RUnlock()

	jobs := make([]*Job, 0, len(p.jobs))
	for _, job := range p.jobs {
		if isItemPresent(job.Name, names) || isItemPresent(job.Tag, tags) {
			jobs = append(jobs, job)
		}
	}

	return jobs
}

func isItemPresent(item string, items []string) bool {
	for _, i := range items {
		if i == "*" || i == item {
			return true
		}
	}
	return false
}

func (p *Pool) isRunning() bool {
	return (atomic.LoadUint32(&p.running) == 1)
}

func (p *Pool) setRunning(running bool) {
	if running {
		atomic.SwapUint32(&p.running, 1)
	} else {
		atomic.SwapUint32(&p.running, 0)
	}
}

func (p *Pool) statDaemon() {
	jobStates := make(map[int64]*JobState)
	ticker := time.NewTicker(2 * time.Second)

	for {
		select {
		case _ = <-ticker.C:
			(*p.flush)(jobStates)
			jobStates = make(map[int64]*JobState)
		case newState, more := <-p.outputQueue:
			if more {
				jobStates[newState.ID] = newState
			} else {
				return
			}
		}
	}
}

// https://youtu.be/pta-gf6JaHQ
func (p *Pool) blackHole() {
	for {
		select {
		case _, more := <-p.outputQueue:
			if !more {
				return
			}
		}
	}
}
