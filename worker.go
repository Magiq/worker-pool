package worker

import (
	"time"
)

func runWorker(jobQueue <-chan *Job, outputQueue chan<- *JobState) {
	for {
		job, more := <-jobQueue
		if more {
			job.setRunning(true)
			wrapper(job, outputQueue)
			job.setRunning(false)
		} else {
			break
		}
	}
}

func wrapper(job *Job, outputQueue chan<- *JobState) {
	now := time.Now()
	jobState := &JobState{TaskID: job.TaskID, StartedAt: &now, State: JobStateWorking}
	defer func() {
		now := time.Now()
		jobState.State = JobStateFinished
		jobState.FinishedAt = &now
		if v := recover(); v != nil {
			// Catch panic in worker
			jobState.Err = v.(error)
		}
		outputQueue <- jobState
	}()

	jobState.Err = job.Routine(func(progress Progress) {
		jobState.Progress = progress
		outputQueue <- jobState
	})
}
