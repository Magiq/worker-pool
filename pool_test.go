package worker

import (
	"testing"
)

func TestRunningAtOnce(t *testing.T) {
	pingPong := make(chan int)
	job1 := CreateJob("job1", "jobs", func(report ReportFn) error {
		return func() error {
			<-pingPong
			pingPong <- 1
			return nil
		}()
	})
	job2 := CreateJob("job2", "jobs", func(report ReportFn) error {
		return func() error {
			<-pingPong
			pingPong <- 1
			return nil
		}()
	})
	pool := CreatePool(10, 10, nil)
	pool.AddJob(job1)
	pool.AddJob(job2)
	pool.Run()

	// Should run both jobs
	pool.Exec([]string{}, []string{"jobs"})
	// Should not run this since it already working
	pool.Exec([]string{"job1"}, []string{})
	pingPong <- 1
	pingPong <- 1
	<-pingPong
	<-pingPong
	// Should run because job2 is over
	pool.Exec([]string{"job2"}, []string{})
	pingPong <- 1
	<-pingPong
}
