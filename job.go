package worker

import (
	"sync/atomic"
	"time"
)

type ReportFn func(progress Progress)

const (
	JobStateWait = iota
	JobStatePending
	JobStateWorking
	JobStateFinished
	JobStateInactive
)

type Job struct {
	Name    string `json:"name"`
	Tag     string `json:"tag"`
	running uint32
	Routine func(ReportFn) error
	JobState
}

type Task struct {
	ID    int64    `json:"id" db:"id"`
	Names []string `json:"names" db:"names"`
	Tags  []string `json:"tags" db:"tags`
}

type JobState struct {
	ID         int64      `json:"id"`
	TaskID     int64      `json:"task_id"`
	StartedAt  *time.Time `json:"finished_at"`
	FinishedAt *time.Time `json:"finished_at"`
	State      int        `json:"state"`
	Err        error      `json:"err"`
	Progress   Progress
}

func CreateJob(name, tag string, routine func(ReportFn) error) *Job {
	return &Job{
		Name:    name,
		Tag:     tag,
		Routine: routine,
	}
}

func (j *Job) isRunning() bool {
	return (atomic.LoadUint32(&j.running) == 1)
}

func (j *Job) setRunning(running bool) {
	if running {
		atomic.SwapUint32(&j.running, 1)
	} else {
		atomic.SwapUint32(&j.running, 0)
	}
}
