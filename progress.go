package worker

type Progress interface {
	Count() int
	Timestamp() int64
}
